import json
import logging.config
import os
import sys
import time
from copy import deepcopy
from pathlib import Path
from typing import List, Callable, Any, Iterable, Union, Set

import yaml

from aio_position_checker import get_files_with_words_positions
from excel_book import ExcelBook


REQUIRED_PYTHON_VERSION = (3, 7)
WORK_DIR = Path(__file__).parent
CONFIG_FILEPATH = Path(os.getenv('CONFIG_FILEPATH', 'work_data/config.yaml'))
LOGGING_FILEPATH = Path(os.getenv('LOGGING_FILEPATH', 'work_data/logging.yaml'))
MAX_COUNT_WORDS_CHECKS = 100


def add_work_dir_to_filepath(filepath: Path, work_dir: Path=WORK_DIR) -> Path:
    if not filepath.is_absolute():
        filepath = Path(work_dir) / filepath
    return filepath


def make_dirs(list_dirs: Iterable[Path]) -> None:
    for new_dir in list_dirs:
        new_dir.mkdir(exist_ok=True)


DOWNLOAD_PATH = add_work_dir_to_filepath(
    Path(os.getenv('DOWNLOAD_PATH', 'downloads')))
make_dirs([DOWNLOAD_PATH])


def parse_file(filepath: Path, parse_func: Callable[..., Any]) -> Any:
    filepath = add_work_dir_to_filepath(filepath)
    with open(filepath) as f:
        return parse_func(f)


def parse_json_file(filepath: Path) -> Any:
    return parse_file(filepath, json.load)


def parse_yaml_file(filepath: Path) -> Any:
    return parse_file(filepath, yaml.safe_load)


def raise_error_if_not_isinstance_from_file(verifiable: Any, expected_type: Any,
                                            filepath: Union[str, Path]) -> None:
    if not isinstance(verifiable, expected_type):
        message = (f"Invalid data structure in file '{filepath}'. "
                   f"The file must contain a '{expected_type}'.")
        logger.error(message)
        raise TypeError(message)


def get_log_dirs_from_config(config: dict) -> Set[Path]:
    log_dirs = set()
    for handlers in config['handlers'].values():
        filename = handlers.get('filename')
        if filename:
            absolute_log_filepath = add_work_dir_to_filepath(Path(filename))
            log_dir = absolute_log_filepath.parent
            log_dirs.add(log_dir)
            handlers['filename'] = absolute_log_filepath
    return log_dirs


def setup_logging(conf_filepath: Path, default_level: int=logging.INFO) -> None:
    try:
        config = parse_yaml_file(conf_filepath)
        log_dirs = get_log_dirs_from_config(config)
        make_dirs(log_dirs)
        logging.config.dictConfig(config)
    except Exception as e:
        print(e)
        print('Error in Logging Configuration. Using default configs.')
        logging.basicConfig(level=default_level)


def get_list_lines_from_file(filepath: Path) -> List[str]:
    filepath = add_work_dir_to_filepath(filepath)
    lines_list = []
    with open(filepath) as file_handler:
        for line in file_handler:
            lines_list.append(line.strip())
    return lines_list


def get_chunk_list(iterable: list, count: int) -> list:
    return [iterable[i:i + count] for i in range(0, len(iterable), count)]


def add_words_for_check(json_data: List[dict]) -> None:
    for url in json_data:
        all_words = set()
        for sheet in url['sheets']:
            words_in_sheet = list(
                set(get_list_lines_from_file(Path(sheet['words_filepath']))))
            sheet['words'] = words_in_sheet
            all_words.update(set(words_in_sheet))
        all_words = sorted(list(all_words))
        chunk_list_words = get_chunk_list(all_words, MAX_COUNT_WORDS_CHECKS)
        url['words_lists_in_parts'] = {}
        list_loaded_files = {}
        for part_num, words_part in enumerate(chunk_list_words, start=1):
            url['words_lists_in_parts'][part_num] = words_part
            list_loaded_files[part_num] = None
        for region in url['regions']:
            region['list_loaded_files'] = deepcopy(list_loaded_files)


def get_dict_words_position_from_file(json_file: Path) -> dict:
    json_data = parse_json_file(json_file)
    raise_error_if_not_isinstance_from_file(json_data, dict, json_file)
    words_by_position = {}
    for word in json_data['result']['queries']:
        words_by_position[word['text']] = word['search_results']
    return words_by_position


def add_words_position_from_files(json_data: List[dict]) -> None:
    for url in json_data:
        for region in url['regions']:
            words_by_position = {}
            for file in region['list_loaded_files'].values():
                words_by_position.update(
                    get_dict_words_position_from_file(file)
                )
            region['words_by_position'] = words_by_position


def add_words_position(json_data: List[dict]) -> None:
    add_words_for_check(json_data)
    get_files_with_words_positions(json_data, DOWNLOAD_PATH)
    add_words_position_from_files(json_data)


def write_data_to_xls(json_data: List[dict]) -> None:
    for url in json_data:
        wb = ExcelBook(add_work_dir_to_filepath(Path(url['result_filepath'])),
                       url['sheets'])
        for sheet in url['sheets']:
            regions = sorted(
                url['regions'],
                key=lambda region: int(region['code']),
                reverse=True,
            )
            for words_for_region in regions:
                wb.add_words_position(
                    sheet_name=sheet['name'],
                    region_name=words_for_region['region_name_in_column'],
                    color_text=str(words_for_region['color_text']),
                    words_by_position=words_for_region['words_by_position'],
                )
        wb.save_wb()
        wb.close_wb()


setup_logging(LOGGING_FILEPATH)
logger = logging.getLogger(__name__)


def raise_error_if_not_required_python_version(
        requided_python_version: tuple) -> None:
    try:
        assert sys.version_info >= requided_python_version, (
            f"Script requires Python "
            f"{'.'.join(map(str, requided_python_version))}+."
        )
    except AssertionError as e:
        logger.exception(e)
        raise e


if __name__ == '__main__':
    start_time = time.perf_counter()
    logger.info('Script is running...')
    raise_error_if_not_required_python_version(REQUIRED_PYTHON_VERSION)
    json_data = parse_yaml_file(CONFIG_FILEPATH)
    raise_error_if_not_isinstance_from_file(json_data, list, CONFIG_FILEPATH)
    add_words_position(json_data)
    write_data_to_xls(json_data)
    logger.info(f"Script completed successfully. Total running time: "
                f"{time.perf_counter() - start_time:0.2f} sec.")
