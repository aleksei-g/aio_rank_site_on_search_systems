import asyncio
import json
import logging
import re
import time
from datetime import datetime
from pathlib import Path
from random import randrange
from typing import List, Union

import aiofiles
from aiohttp import ClientSession, ClientError, http_exceptions


logger = logging.getLogger(__name__)

URL_INITIAL = 'https://be1.ru/position-yandex-google/init'
URL_GET_RESULT = 'https://be1.ru/ajax/tools/get-result'
JSON_FILE_SUFFIX = '.json'
MAX_WAITING_TIME_KEY = 600
SLEEP_TIME_KEY_IS_NONE = 1
RANGE_SLEEP_TIME_KEY_ERROR = (10, 60)
RANGE_SLEEP_TIME_HTTP_EXCEPTION = (60, 60*5)
MAX_WAITING_TIME_HTTP = 3600*6
DELAY_BEFORE_REQUEST_DELTA = 10

HEADERS = {
    'Origin': 'https://be1.ru',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36'
                  ' (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36',
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    'Accept': 'application/json, text/javascript, */*; q=0.01',
    'Referer': 'https://be1.ru/position-yandex-google/?s=e2b4706e2cd26856',
    'X-Requested-With': 'XMLHttpRequest',
    'Connection': 'keep-alive',
}


def check_html_status(func):
    async def wrapped(*args, **kwargs):
        task_str = kwargs.get('task_str', '')
        start_time = time.perf_counter()
        while time.perf_counter() - start_time <= MAX_WAITING_TIME_HTTP:
            try:
                return await func(*args, **kwargs)
            except (
                ClientError,
                http_exceptions.HttpProcessingError,
            ) as e:
                err = e
                sleep_time = randrange(*RANGE_SLEEP_TIME_HTTP_EXCEPTION)
                logger.error(
                    f"{task_str}: aiohttp exception: {e}. SLEEP {sleep_time}."
                )
                await asyncio.sleep(sleep_time)
            except Exception as e:
                logger.exception(
                    f"{task_str}: Non-aiohttp exception occurred: {e}."
                )
                raise e
        else:
            logger.error(
                f"{task_str}: Wait time ({MAX_WAITING_TIME_HTTP} sec.) is over."
            )
            raise err
    return wrapped


def check_key_is_not_none(
        key,
        wait_sec=MAX_WAITING_TIME_KEY,
        sleep_time_if_none=SLEEP_TIME_KEY_IS_NONE,
        range_sleep_time_if_key_error=RANGE_SLEEP_TIME_KEY_ERROR,
):
    def decorator(func):
        async def wrapped(*args, **kwargs):
            task_str = kwargs.get('task_str', '')
            start_time = time.perf_counter()
            while time.perf_counter() - start_time <= wait_sec:
                try:
                    result = await func(*args, **kwargs)
                    if result[key] is None:
                        logger.debug(
                            f"{task_str}: '{key}' is None. "
                            f"SLEEP {sleep_time_if_none}]."
                        )
                        await asyncio.sleep(sleep_time_if_none)
                    else:
                        return result
                except KeyError as e:
                    sleep_time_key_error = randrange(
                        *range_sleep_time_if_key_error
                    )
                    logger.debug(
                        f"{task_str}: KeyError exception: {e}. "
                        f"SLEEP {sleep_time_key_error}."
                    )
                    await asyncio.sleep(sleep_time_key_error)
                except Exception as e:
                    logger.exception(
                        f"{task_str}: Non-KeyError exception occurred: {e}."
                    )
                    raise e
            else:
                logger.error(
                    f"{task_str}: Wait time ({wait_sec} sec.) is over. "
                    f"Key '{key}' does not exist or {key} is None."
                )
                raise KeyError
        return wrapped
    return decorator


def get_site_name_from_url(url: str) -> str:
    site_name = re.compile(r"https?://(www\.)?")
    return site_name.sub('', url).strip().strip('/')


def build_file_name(path_dir: Path, url_check: str,
                    code_region: Union[int, str], part_num: Union[int, str],
                    suffix: str) -> Path:
    name_str = (f"{datetime.now():%Y-%m-%d_%H-%M-%S}_"
                f"{get_site_name_from_url(url_check)}_"
                f"{code_region}_p{part_num}"
                f"{suffix}")
    new_filepath = path_dir.joinpath(name_str)
    return new_filepath


@check_html_status
async def fetch_json(session: ClientSession, method: str, url: str, task_str,
                     **kwargs) -> dict:
    response = await session.request(method=method, url=url, **kwargs)
    response.raise_for_status()
    return await response.json()


@check_key_is_not_none('slug')
async def get_slug_json(session: ClientSession, url: str, region: str,
                        words_part: list, task_str) -> dict:
    payload = {
        'queries': '\n'.join(words_part),
        'domain': url,
        'region': region,
        'engines[]': 'yandex'
    }
    response_json = await fetch_json(
        session=session,
        method='POST',
        url=URL_INITIAL,
        data=payload,
        headers=HEADERS,
        task_str=task_str,
    )
    return response_json


@check_key_is_not_none('result')
async def get_result_json(session: ClientSession, slug: str,
                          task_str: str) -> dict:
    params = {
        't': 'position-yandex-google',
        's': slug,
    }
    response_json = await fetch_json(
        session=session,
        method='GET',
        url=URL_GET_RESULT,
        params=params,
        headers=HEADERS,
        task_str=task_str,
    )
    return response_json


async def get_file_with_positions(session: ClientSession, url: str,
                                  region: dict, part_num: Union[int, str],
                                  words_part: List[str], task_str: str,
                                  download_path: Path,
                                  delay_before_request: int=0) -> Path:
    logger.debug(
        f"{task_str}: SLEEP before request {delay_before_request} sec."
    )
    await asyncio.sleep(delay_before_request)
    start_time_in_step = time.perf_counter()
    slug_json = await get_slug_json(
        session=session, url=url, region=region['code'],
        words_part=words_part, task_str=task_str)
    result_json = await get_result_json(
        session=session, slug=slug_json.get('slug'), task_str=task_str)
    file_path = build_file_name(download_path, url, region['code'],
                                part_num, JSON_FILE_SUFFIX)
    async with aiofiles.open(file_path, "w") as f:
        await f.write(json.dumps(result_json))
    logger.info(f"{task_str}: result json save to file: {file_path}.")
    logger.info(f"{task_str}: Time spent: "
                f"{time.perf_counter() - start_time_in_step:0.2f} sec.")
    region['list_loaded_files'][part_num] = file_path
    return file_path


async def check_words(json_data: List[dict], download_path: Path) -> List[Path]:
    async with ClientSession() as session:
        tasks = []
        for url in json_data:
            for region in url['regions']:
                for part_num, loaded_file in region[
                    'list_loaded_files'].items():
                    words_list_in_part = url['words_lists_in_parts'][part_num]
                    task_str = (f"url={url['url']},\tregion={region['code']},"
                                f"\tpart_num={part_num}")
                    delay_before_request = (
                        len(tasks) * DELAY_BEFORE_REQUEST_DELTA)
                    tasks.append(get_file_with_positions(
                        session, url['url'], region, part_num,
                        words_list_in_part, task_str, download_path,
                        delay_before_request))
        return await asyncio.gather(*tasks)


def get_files_with_words_positions(json_data: List[dict],
                                   download_path: Path) -> None:
    start_time = time.perf_counter()
    asyncio.run(check_words(json_data, download_path))
    logger.info(
        f"Total running time: {time.perf_counter() - start_time:0.2f} sec.")
